<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape([
        'dependencies' => 'array',
        'doctrine' => 'array',
        'routes' => 'array',
        'i18n' => 'array',
    ])] public function __invoke(): array
    {
        return [
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'doctrine' => include __DIR__ . '/../config/doctrine.php',
            'routes' => include __DIR__ . '/../config/routes.php',
            'i18n' => [
                'territorial-division' => include __DIR__ . '/../config/i18n-territorial-division.php',
            ],
        ];
    }
}
