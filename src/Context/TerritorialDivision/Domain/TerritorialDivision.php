<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision\Context\TerritorialDivision\Domain;

use Smtm\Base\Domain\AbstractUuidAwareEntity;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TerritorialDivision extends AbstractUuidAwareEntity
{
    protected string $name;
    protected string $officialStateName;
    protected string $sovereignty;
    protected string $codeIso3166Alpha2;
    protected string $codeIso3166Alpha3;
    protected string $codeIso3166Numeric;
    protected string $ccTld;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getOfficialStateName(): string
    {
        return $this->officialStateName;
    }

    public function setOfficialStateName(string $officialStateName): static
    {
        $this->officialStateName = $officialStateName;

        return $this;
    }

    public function getSovereignty(): string
    {
        return $this->sovereignty;
    }

    public function setSovereignty(string $sovereignty): static
    {
        $this->sovereignty = $sovereignty;

        return $this;
    }

    public function getCodeIso3166Alpha2(): string
    {
        return $this->codeIso3166Alpha2;
    }

    public function setCodeIso3166Alpha2(string $codeIso3166Alpha2): static
    {
        $this->codeIso3166Alpha2 = $codeIso3166Alpha2;

        return $this;
    }

    public function getCodeIso3166Alpha3(): string
    {
        return $this->codeIso3166Alpha3;
    }

    public function setCodeIso3166Alpha3(string $codeIso3166Alpha3): static
    {
        $this->codeIso3166Alpha3 = $codeIso3166Alpha3;

        return $this;
    }

    public function getCodeIso3166Numeric(): string
    {
        return $this->codeIso3166Numeric;
    }

    public function setCodeIso3166Numeric(string $codeIso3166Numeric): static
    {
        $this->codeIso3166Numeric = $codeIso3166Numeric;

        return $this;
    }

    public function getCcTld(): string
    {
        return $this->ccTld;
    }

    public function setCcTld(string $ccTld): static
    {
        $this->ccTld = $ccTld;

        return $this;
    }
}
