<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision\Context\TerritorialDivision\Application\Hydrator;

use Smtm\Base\Application\Hydrator\DomainObjectHydrator;

class TerritorialDivisionHydrator extends DomainObjectHydrator
{
    /** @inheritdoc  */
    protected array $mustHydrate = [
        'name' => 'You must specify a name for the TerritorialDivision.',
        'officialStateName' => 'You must specify an officialStateName for the TerritorialDivision.',
        'sovereignty' => 'You must specify sovereignty for the TerritorialDivision.',
        'codeIso3166Alpha2' => 'You must specify a codeIso3166Alpha2 for the TerritorialDivision.',
        'codeIso3166Alpha3' => 'You must specify a codeIso3166Alpha3 for the TerritorialDivision.',
        'codeIso3166Numeric' => 'You must specify a codeIso3166Numeric for the TerritorialDivision.',
        'ccTld' => 'You must specify a ccTld for the TerritorialDivision.',
    ];
}
