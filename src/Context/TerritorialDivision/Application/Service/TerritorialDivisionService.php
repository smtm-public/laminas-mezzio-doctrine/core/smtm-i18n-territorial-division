<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision\Context\TerritorialDivision\Application\Service;

use Smtm\Base\Application\Service\ApplicationService\AbstractDbService;
use Smtm\Base\Application\Service\ApplicationService\DbConfigurableEntityManagerNameServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceInterface;
use Smtm\Base\Application\Service\ApplicationService\DbService\UuidAwareEntityDbServiceTrait;
use Smtm\Base\ConfigAwareTrait;
use Smtm\I18n\TerritorialDivision\Context\TerritorialDivision\Application\Hydrator\TerritorialDivisionHydrator;
use Smtm\I18n\TerritorialDivision\Context\TerritorialDivision\Domain\TerritorialDivision;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TerritorialDivisionService extends AbstractDbService implements
    UuidAwareEntityDbServiceInterface,
    DbConfigurableEntityManagerNameServiceInterface
{

    use UuidAwareEntityDbServiceTrait, ConfigAwareTrait;

    protected ?string $domainObjectName = TerritorialDivision::class;
    protected ?string $hydratorName = TerritorialDivisionHydrator::class;
}
