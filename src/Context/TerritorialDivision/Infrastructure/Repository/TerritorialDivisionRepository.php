<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision\Context\TerritorialDivision\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Doctrine\Orm\AbstractRepository;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class TerritorialDivisionRepository extends AbstractRepository implements
    TerritorialDivisionRepositoryInterface
{

}
