<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision\Context\TerritorialDivision\Infrastructure\Repository;

use Smtm\Base\Infrastructure\Repository\RepositoryInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface TerritorialDivisionRepositoryInterface extends RepositoryInterface
{

}
