<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision\Factory;

use Smtm\Base\Factory\ConfigAwareDelegator;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class I18nTerritorialDivisionConfigAwareDelegator extends ConfigAwareDelegator
{
    protected array|string|null $configKey = ['i18n', 'territorial-division'];
}
