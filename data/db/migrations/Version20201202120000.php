<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision\Migration;

use Smtm\Base\Infrastructure\Doctrine\Migration\UuidStringColumnAndUniqueIndexMigrationTrait;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class Version20201202120000 extends AbstractMigration
{

    use UuidStringColumnAndUniqueIndexMigrationTrait;

    public function up(Schema $schema): void
    {
        $this->createI18nTerritorialDivisionTable($schema);
    }

    public function createI18nTerritorialDivisionTable(Schema $schema): void
    {
        $i18nTerritorialDivisionTable = $schema->createTable('i18n_territorial_division');
        $i18nTerritorialDivisionTable->addColumn('id', Types::INTEGER);
        $i18nTerritorialDivisionTable->setPrimaryKey(['id']);
        $this->addUuidStringColumnAndUniqueIndex($i18nTerritorialDivisionTable);
        $i18nTerritorialDivisionTable->addColumn(
            'r_name', Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $i18nTerritorialDivisionTable->addUniqueIndex(
            ['r_name'],
            'idx_unq_' . $i18nTerritorialDivisionTable->getName() . '_r_name'
        );
        $i18nTerritorialDivisionTable->addColumn(
            'official_state_name', Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $i18nTerritorialDivisionTable->addUniqueIndex(
            ['official_state_name'],
            'idx_unq_' . $i18nTerritorialDivisionTable->getName() . '_official_state_name'
        );
        $i18nTerritorialDivisionTable->addColumn(
            'sovereignty', Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
        $i18nTerritorialDivisionTable->addColumn(
            'code_iso3166_alpha2', Types::STRING,
            ['length' => 2, 'notNull' => true]
        );
        $i18nTerritorialDivisionTable->addUniqueIndex(
            ['code_iso3166_alpha2'],
            'idx_unq_' . $i18nTerritorialDivisionTable->getName() . '_code_iso3166_alpha2'
        );
        $i18nTerritorialDivisionTable->addColumn(
            'code_iso3166_alpha3', Types::STRING,
            ['length' => 3, 'notNull' => true]
        );
        $i18nTerritorialDivisionTable->addUniqueIndex(
            ['code_iso3166_alpha3'],
            'idx_unq_' . $i18nTerritorialDivisionTable->getName() . '_code_iso3166_alpha3'
        );
        $i18nTerritorialDivisionTable->addColumn(
            'code_iso3166_numeric', Types::STRING,
            ['length' => 3, 'notNull' => true]
        );
        $i18nTerritorialDivisionTable->addUniqueIndex(
            ['code_iso3166_numeric'],
            'idx_unq_' . $i18nTerritorialDivisionTable->getName() . '_numeric'
        );
        $i18nTerritorialDivisionTable->addColumn(
            'cc_tld',
            Types::STRING,
            ['length' => 255, 'notNull' => true]
        );
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('i18n_territorial_division');
    }
}
