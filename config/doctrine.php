<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision;

return [
    'orm' => [
        'mapping' => [
            'paths' => [
                \Smtm\I18n\TerritorialDivision\Context\TerritorialDivision\Domain\TerritorialDivision::class =>
                    __DIR__ . '/../src/Context/TerritorialDivision/Infrastructure/Doctrine/Orm/Mapping',
            ],
        ],
    ],
];
