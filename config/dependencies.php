<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision;

use Smtm\Base\Application\Service\ApplicationService\Factory\DbConfigurableEntityManagerNameServiceDelegator;
use Smtm\Base\Application\Service\ApplicationServicePluginManager;
use Smtm\I18n\TerritorialDivision\Context\TerritorialDivision\Application\Service\TerritorialDivisionService;
use Smtm\I18n\TerritorialDivision\Factory\I18nTerritorialDivisionConfigAwareDelegator;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        ApplicationServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var ApplicationServicePluginManager $applicationServicePluginManager */
                $applicationServicePluginManager = $callback();

                $applicationServicePluginManager->addDelegator(
                    TerritorialDivisionService::class,
                    I18nTerritorialDivisionConfigAwareDelegator::class
                );
                $applicationServicePluginManager->addDelegator(
                    TerritorialDivisionService::class,
                    DbConfigurableEntityManagerNameServiceDelegator::class
                );

                return $applicationServicePluginManager;
            }
        ],
    ],
];
