<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision;

return [
    'em' => 'default',

    'table_storage' => [
        'table_name' => 'doctrine_migrations_smtm_i18n_territorial_division',
    ],

    'migrations_paths' => [
        'Smtm\I18n\TerritorialDivision\Migration' => __DIR__ . '/../data/db/migrations',
    ],
];
