<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-i18n-territorial-division')) {
    $dotenv = \Dotenv\Dotenv::createMutable(__DIR__ . '/../../../../', '.env.smtm.smtm-i18n-territorial-division');
    $dotenv->load();
}

return [
    'entityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_I18N_TERRITORIAL_DIVISION_DB_ENTITY_MANAGER_NAME'),
    'readerEntityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_I18N_TERRITORIAL_DIVISION_DB_READER_ENTITY_MANAGER_NAME'),
    'archivedAccessEntityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_I18N_TERRITORIAL_DIVISION_DB_ARCHIVED_ACCESS_ENTITY_MANAGER_NAME'),
    'archivedAccessReaderEntityManagerName' => EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_I18N_TERRITORIAL_DIVISION_DB_ARCHIVED_ACCESS_READER_ENTITY_MANAGER_NAME'),
];
