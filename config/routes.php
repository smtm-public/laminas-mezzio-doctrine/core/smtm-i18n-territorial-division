<?php

declare(strict_types=1);

namespace Smtm\I18n\TerritorialDivision;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-i18n-territorial-division')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-i18n-territorial-division'
    );
    $dotenv->load();
}

$exposedRoutes = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_I18N_TERRITORIAL_DIVISION_EXPOSED_ROUTES', '[]'),
    true
);
$exposedRoutes = array_combine($exposedRoutes, $exposedRoutes);

$routes = [
    'smtm.i18n.territorial-division.territorial-division.read' => [
        'path' => '/i18n/territorial-division/territorial-division/{uuid}',
        'method' => 'get',
        'middleware' => Context\TerritorialDivision\Http\Handler\ReadHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
    'smtm.i18n.territorial-division.territorial-division.index' => [
        'path' => '/i18n/territorial-division/territorial-division',
        'method' => 'get',
        'middleware' => Context\TerritorialDivision\Http\Handler\IndexHandler::class,
        'options' => [
            'authentication' => null,
            'authorization' => null,
            'requestValidatingInputFilterCollectionName' => null,
        ],
    ],
];

return array_intersect_key($routes, $exposedRoutes);
